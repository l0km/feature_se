:: Android NDK 交叉编译脚本(armeabi) 
:: author guyadong
:: 2020/12/21
SETLOCAL
SET sh_folder=%~dp0

SET ANDROID_ABI=armeabi
%sh_folder%ndk_build_mtfsdk.bat

ENDLOCAL