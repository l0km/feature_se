echo off 
SETLOCAL
echo make feature_se VS2015 project
if not defined VS140COMNTOOLS (
	echo vs2015 NOT FOUND.
	exit /B -1
)
echo vs2015 found.
where cmake
if errorlevel 1 (
	echo cmake NOT FOUND.
	exit /B -1
)
echo cmake found
where java
if errorlevel 1 (
	echo java NOT FOUND.
	exit /B -1
)
echo java found
set sh_folder=%~dp0
set PREFIX=%sh_folder%release\fse_mtfsdk_windows_x86_64
set LIB_NAME=FS_FaceFeatureCompare
set OPT_BUILD=1

:: parse command arguments
:loop
IF x%1 == x goto :pare_end
IF /I "%1" == "/NOBUILD"      SET OPT_BUILD=0
SHIFT
goto :loop
:pare_end
pushd %sh_folder%

if exist project.mtfsdk.vs2015 rmdir project.mtfsdk.vs2015 /s/q
mkdir project.mtfsdk.vs2015
pushd project.mtfsdk.vs2015
if not defined VisualStudioVersion (
	echo make MSVC environment ...
	call "%VS140COMNTOOLS%..\..\vc/vcvarsall" x86_amd64
)

@rem 编译基于MTFSDK windows(x86_64) 的feature_se动态库
@rem EXT_SDK_TYPE 指定算法类型可选值： 
@rem                    CASSDK(默认值) 
@rem                    EUCLIDEAN  默认使用欧氏距离计算相似度 
@rem                    CUSTOM   使用自定义算法提的供相似度比较函数 
@rem 如果EXT_SDK_TYPE指定为EUCLIDEAN,下列参数需要设置:
@rem EUCLIDEAN_ELEM_TYPE 定义特征值数组类型(double/float)，如果不指定，默认值为double 
@rem EUCLIDEAN_ELEM_LEN  定义特征值数组长度 
@rem EUCLIDEAN_CODE_END_WITH_SUM  定义特征值数组最后是否有一个double保存特征值数组的点积和，默认为OFF 
@rem ============================下列为通用参数与EXT_SDK_TYPE无关
@rem FSE_LIBNAME        指定生成动态库名,不指定则使用默认值 
@rem JNI_FSE_LIBNAME    指定生成jni动态库名,不指定则使用默认值 

echo creating x86_64 Project for Visual Studio 2015 ...
cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_INSTALL_PREFIX=%PREFIX% .. ^
	-DJNI_FSE_LIBNAME=%LIB_NAME% ^
	-DEXT_SDK_TYPE=EUCLIDEAN ^
	-DEUCLIDEAN_ELEM_TYPE=float ^
	-DEUCLIDEAN_ELEM_LEN=128 ^
	-DEUCLIDEAN_CODE_END_WITH_SUM=OFF	

SET dst_dir=%sh_folder%..\mtfsdk\mtfsdk-base\src\main\resources\win32-x86-64

IF %OPT_BUILD% == 1 (
  CMAKE.EXE --build . --config RELEASE --target INSTALL -- /maxcpucount || EXIT /B -1
  IF EXIST %sh_folder%..\mtfsdk (
      IF NOT EXIST %dst_dir% ( MKDIR %dst_dir% || EXIT /B )
      COPY /Y /V %PREFIX%\bin\FS_FaceFeatureCompare.dll %dst_dir%
  )
)
popd
popd
ENDLOCAL