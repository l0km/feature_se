:: Android NDK 交叉编译脚本,创建Makefile
:: author guyadong
:: 可选参数:
::   /NOBUILD        - 只生成Makefile工程文件,不编译项目 
::   /DEBUG          - 生成Debug版工程 
:: 编译 arm5(armeabi) 平台版本要求　NDK 16 及以下版本.

@ECHO OFF
SETLOCAL
:: 检测是否安装NDK,没有安装NDK则报错退出
IF NOT DEFINED ANDROID_NDK (
    ECHO "ERROR:environment variable ANDROID_NDK not define" && exit /B -1
    )
where cmake
IF ERRORLEVEL 1 (
    ECHO cmake NOT FOUND.
    EXIT /B -1
)
echo cmake found
SET sh_folder=%~dp0
:: 定义编译的版本类型(DEBUG|RELEASE)
SET build_type=RELEASE

SET OPT_BUILD=1
:: parse command arguments
:loop
IF x%1 == x goto :pare_end
IF /I "%1" == "/NOBUILD"    SET OPT_BUILD=0
IF /I "%1" == "/DEBUG"      SET build_type=Debug
SHIFT
goto :loop
:pare_end

ECHO build_type=%build_type%

:: 目标平台
IF NOT DEFINED ANDROID_ABI SET ANDROID_ABI=armeabi-v7a

IF %ANDROID_ABI%==armeabi ( 
    SET ARCH=ARMEABI
) ELSE IF %ANDROID_ABI%==armeabi-v7a ( 
    SET ARCH=ARMEABI-V7A
) ELSE IF %ANDROID_ABI%==arm64-v8a (
    SET ARCH=ARM64-V8A
) ELSE IF %ANDROID_ABI%==x86 (
    SET ARCH=X86
) ELSE IF %ANDROID_ABI%==x86_64 (
    SET ARCH=x64
) ELSE (
      ECHO Invalid Android ABI: %ANDROID_ABI%
      EXIT /B 255
)

:: 编译输出路径
SET BUILDDIR=%sh_folder%build\fse_mtfsdk_anrdoid\%ANDROID_ABI%
SET OUTDIR=%sh_folder%release\fse_mtfsdk_anrdoid\%ANDROID_ABI%

ECHO =========================================================================
ECHO     Configuration: fse_mtfsdk %ANDROID_ABI%:%build_type%
ECHO   Build Directory: %BUILDDIR%
ECHO Install Directory: %OUTDIR%
ECHO =========================================================================

IF EXIST "%BUILDDIR%" ( RMDIR "%BUILDDIR%" /s/q || EXIT /B )
MKDIR "%BUILDDIR%" || EXIT /B

PUSHD "%BUILDDIR%" || EXIT /B

@REM EXT_SDK_TYPE 指定算法类型可选值： 
@REM                    CASSDK(默认值) 
@REM                    EUCLIDEAN  默认使用欧氏距离计算相似度 
@REM                    CUSTOM   使用自定义算法提的供相似度比较函数 
@REM 如果EXT_SDK_TYPE指定为EUCLIDEAN,下列参数需要设置:
@REM EUCLIDEAN_ELEM_TYPE 定义特征值数组类型(double/float)，如果不指定，默认值为double 
@REM EUCLIDEAN_ELEM_LEN  定义特征值数组长度 
@REM EUCLIDEAN_CODE_END_WITH_SUM  定义特征值数组最后是否有一个double保存特征值数组的点积和，默认为OFF 
@REM ============================下列为通用参数与EXT_SDK_TYPE无关
@REM FSE_LIBNAME        指定生成动态库名,不指定则使用默认值 
@REM JNI_FSE_LIBNAME    指定生成jni动态库名,不指定则使用默认值 

cmake %sh_folder% -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=%build_type% ^
    -DJNI_FSE_LIBNAME=FS_FaceFeatureCompare ^
    -DEXT_SDK_TYPE=EUCLIDEAN ^
    -DEUCLIDEAN_ELEM_TYPE=float ^
    -DEUCLIDEAN_ELEM_LEN=128 ^
    -DEUCLIDEAN_CODE_END_WITH_SUM=OFF ^
    -DCMAKE_SYSTEM_VERSION=17 ^
    -DCMAKE_INSTALL_PREFIX=%OUTDIR% ^
	-DANDROID_STL=c++_static ^
    -DANDROID_ABI=%ANDROID_ABI% ^
    -DCMAKE_TOOLCHAIN_FILE=%ANDROID_NDK%\build\cmake\android.toolchain.cmake || EXIT /B

:: 加入NEON指令优化后 会导致release版本执行异常
::     -DANDROID_ARM_NEON=ON 

SET dst_dir=%sh_folder%..\mtfsdk-android-common\src\main\resources\lib\%ANDROID_ABI%

IF %OPT_BUILD% == 1 (
  cmake --build . --target install/strip -- -j8 || EXIT /B
  IF EXIST %sh_folder%..\mtfsdk-android-common (
      IF NOT EXIST %dst_dir% ( MKDIR %dst_dir% || EXIT /B )
      COPY /Y /V %OUTDIR%\lib\libFS_FaceFeatureCompare.so %dst_dir%
  )
)

POPD
ENDLOCAL